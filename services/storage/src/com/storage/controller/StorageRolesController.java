package com.storage.controller; 

// Generated Sep 5, 2014 11:09:56 AM


import com.storage.service.RolesService;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.expression.QueryFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.storage.*;
import org.springframework.web.bind.annotation.PathVariable;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;

/**
 * Controller object for domain model class Roles.
 * @see com.storage.Roles
 */

@RestController
@RequestMapping("/storage/Roles")
public class StorageRolesController {

    private static final Logger LOGGER = LoggerFactory.getLogger(StorageRolesController.class);

	@Autowired
	@Qualifier("storage.RolesService")
	private RolesService service;

	/**
	 * Processes requests to return lists all available Roless.
	 * 
	 * @param model
	 * @return The name of the  Roles list view.
	 */
	 
	@RequestMapping(value = "/search", method = RequestMethod.POST)
	public Page<Roles> findAll(
			@RequestBody QueryFilter[] queryFilters,
			@RequestParam(defaultValue = "1") int page,
			@RequestParam(defaultValue = "20") int size) {
		LOGGER.debug("Rendering Roless list");
		Pageable pageable = new PageRequest(page - 1, size);
		return service.findAll(queryFilters, pageable);
	}
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public Page<Roles> getRoless(
			@RequestParam(defaultValue = "1") int page,
			@RequestParam(defaultValue = "20") int size) {
		LOGGER.debug("Rendering Roless list");
		Pageable pageable = new PageRequest(page - 1, size);
		return service.findAll(pageable);
	}
	
	@RequestMapping(value = "/count", method = RequestMethod.GET)
	public Long countAll() {
		LOGGER.debug("counting Roless");
		Long count = service.countAll();
		return count;
	}


    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    	public Roles getRoles(@PathVariable("id") Integer id) throws EntityNotFoundException {
    		LOGGER.debug("Getting Roles with id: {}" , id);
    		Roles instance = service.findById(id);
    		LOGGER.debug("Roles details with id: {}" , instance);
    		return instance;
    	}

    	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    	public boolean delete(@PathVariable("id") Integer id) throws EntityNotFoundException {
    		LOGGER.debug("Deleting Roles with id: {}" , id);
    		Roles deleted = service.delete(id);
    		return deleted != null;
    	}

    	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    	public Roles editRoles(@PathVariable("id") Integer id, @RequestBody Roles instance) throws EntityNotFoundException {
            LOGGER.debug("Editing Roles with id: {}" , instance.getRoleId());
            instance.setRoleId(id);
    		instance = service.update(instance);
    		LOGGER.debug("Roles details with id: {}" , instance);
    		return instance;
    	}



	@RequestMapping(value = "/", method = RequestMethod.POST)
	public Roles createRoles(@RequestBody Roles instance) {
		LOGGER.debug("Create Roles with information: {}" , instance);
		instance = service.create(instance);
		LOGGER.debug("Created Roles with information: {}" , instance);
	    return instance;
	}
	
	/**
	 * This setter method should only be used by unit tests
	 * 
	 * @param service
	 */
	protected void setRolesService(RolesService service) {
		this.service = service;
	}
}

