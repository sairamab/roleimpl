
package com.storage.service;
// Generated Sep 8, 2014 5:20:16 AM 

import java.util.Map;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.wavemaker.runtime.data.model.CustomQuery;
import com.wavemaker.runtime.data.exception.QueryParameterMismatchException;

public interface QueryExecutorService {
    Page<Object> executeQuery(Pageable pageable, java.lang.Integer id) throws QueryParameterMismatchException;
	int executeQuery2( java.lang.String n, java.lang.String e, java.lang.String p) throws QueryParameterMismatchException;

	
	Page<Object> executeWMCustomQuerySelect(CustomQuery query, Pageable pageable) ;

	int executeWMCustomQueryUpdate(CustomQuery query) ;
}

