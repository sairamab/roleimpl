
package com.storage.service;
// Generated Sep 8, 2014 5:20:16 AM 


import java.util.HashMap;
import java.util.Map;
import java.util.Collections;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.wavemaker.runtime.data.model.CustomQuery;
import com.wavemaker.runtime.data.dao.query.WMQueryExecutor;
import com.wavemaker.runtime.data.exception.QueryParameterMismatchException;

@Service("storage.queryExecutorService")
public class QueryExecutorServiceImpl implements QueryExecutorService {
	private static final Logger LOGGER = LoggerFactory.getLogger(QueryExecutorServiceImpl.class);

	@Autowired
	@Qualifier("storageWMQueryExecutor")
	private WMQueryExecutor queryExecutor;

	@Transactional(value = "storageTransactionManager")
	@Override
	public Page<Object> executeQuery(Pageable pageable, java.lang.Integer id)
	throws QueryParameterMismatchException{
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("id", id);
        return queryExecutor.executeNamedQuery("query", params, pageable);
	}
	@Transactional(value = "storageTransactionManager")
	@Override
	public int executeQuery2( java.lang.String n, java.lang.String e, java.lang.String p)
	throws QueryParameterMismatchException{
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("n", n);
        params.put("e", e);
        params.put("p", p);
        return queryExecutor.executeNamedQueryForUpdate("query2", params);
	}

	@Transactional(value = "storageTransactionManager")
	@Override
	public Page<Object> executeWMCustomQuerySelect(CustomQuery query, Pageable pageable) {
	    return queryExecutor.executeCustomQuery(query, pageable);
	}

	@Transactional(value = "storageTransactionManager")
    @Override
    public int executeWMCustomQueryUpdate(CustomQuery query) {
        return queryExecutor.executeCustomQueryForUpdate(query);
    }
}

