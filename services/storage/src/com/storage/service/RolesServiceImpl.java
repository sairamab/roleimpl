package com.storage.service;
// Generated Sep 5, 2014 11:09:56 AM

import com.wavemaker.runtime.data.expression.QueryFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.storage.*;
import com.wavemaker.runtime.data.dao.*;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;

/**
 * ServiceImpl object for domain model class Roles.
 * @see com.storage.Roles
 */
@Service("storage.RolesService")
public class RolesServiceImpl implements RolesService {


    private static final Logger LOGGER = LoggerFactory.getLogger(RolesServiceImpl.class);


@Autowired
@Qualifier("storage.RolesDao")
private WMGenericDao<Roles, Integer> wmGenericDao;
  public void setWMGenericDao(WMGenericDao<Roles, Integer> wmGenericDao){
          this.wmGenericDao = wmGenericDao;
  }

    @Transactional(value = "storageTransactionManager")
    @Override
    public Roles create(Roles roles) {
        LOGGER.debug("Creating a new roles with information: {}" , roles);
        return this.wmGenericDao.create(roles);
    }

    @Transactional(rollbackFor = EntityNotFoundException.class, value = "storageTransactionManager")
    @Override
    public Roles delete(Integer rolesId) throws EntityNotFoundException {
        LOGGER.debug("Deleting roles with id: {}" , rolesId);
        Roles deleted = this.wmGenericDao.findById(rolesId);
        if (deleted == null) {
            LOGGER.debug("No roles found with id: {}" , rolesId);
            throw new EntityNotFoundException(String.valueOf(rolesId));
        }
        this.wmGenericDao.delete(deleted);
        return deleted;
    }

    @Transactional(readOnly = true, value = "storageTransactionManager")
    @Override
    public Page<Roles> findAll(QueryFilter[] queryFilters, Pageable pageable) {
        LOGGER.debug("Finding all roless");
        return this.wmGenericDao.search(queryFilters, pageable);
    }
    
    @Transactional(readOnly = true, value = "storageTransactionManager")
    @Override
    public Page<Roles> findAll(Pageable pageable) {
        LOGGER.debug("Finding all roless");
        return this.wmGenericDao.search(null, pageable);
    }

    @Transactional(readOnly = true, value = "storageTransactionManager")
    @Override
    public Roles findById(Integer id) throws EntityNotFoundException {
        LOGGER.debug("Finding roles by id: {}" , id);
        Roles roles=this.wmGenericDao.findById(id);
        if(roles==null){
            LOGGER.debug("No roles found with id: {}" , id);
            throw new EntityNotFoundException(String.valueOf(id));
        }
        return roles;
    }

    @Transactional(rollbackFor = EntityNotFoundException.class, value = "storageTransactionManager")
    @Override
    public Roles update(Roles updated) throws EntityNotFoundException {
        LOGGER.debug("Updating roles with information: {}" , updated);
        this.wmGenericDao.update(updated);
        return this.wmGenericDao.findById((Integer)updated.getRoleId());
    }

    @Transactional(readOnly = true, value = "storageTransactionManager")
    @Override
    public long countAll() {
        return this.wmGenericDao.count();
    }
}


