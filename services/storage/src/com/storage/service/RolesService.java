package com.storage.service;
// Generated Sep 5, 2014 11:09:56 AM


import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.expression.QueryFilter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.storage.*;
/**
 * Service object for domain model class Roles.
 * @see com.storage.Roles
 */

public interface RolesService {

   /**
	 * Creates a new roles.
	 * 
	 * @param created
	 *            The information of the created roles.
	 * @return The created roles.
	 */
	public Roles create(Roles created);

	/**
	 * Deletes a roles.
	 * 
	 * @param rolesId
	 *            The id of the deleted roles.
	 * @return The deleted roles.
	 * @throws EntityNotFoundException
	 *             if no roles is found with the given id.
	 */
	public Roles delete(Integer rolesId) throws EntityNotFoundException;

	/**
	 * Finds all roless.
	 * 
	 * @return A list of roless.
	 */
	public Page<Roles> findAll(QueryFilter[] queryFilters, Pageable pageable);
	
	public Page<Roles> findAll(Pageable pageable);
	
	/**
	 * Finds roles by id.
	 * 
	 * @param id
	 *            The id of the wanted roles.
	 * @return The found roles. If no roles is found, this method returns
	 *         null.
	 */
	public Roles findById(Integer id) throws EntityNotFoundException;

	/**
	 * Updates the information of a roles.
	 * 
	 * @param updated
	 *            The information of the updated roles.
	 * @return The updated roles.
	 * @throws EntityNotFoundException
	 *             if no roles is found with given id.
	 */
	public Roles update(Roles updated) throws EntityNotFoundException;

	/**
	 * Retrieve the total count of the roless in the repository.
	 * 
	 * @param None
	 *            .
	 * @return The count of the roles.
	 */

	public long countAll();

}

